# Complete project details at https://RandomNerdTutorials.com
import ure
import machine

def html_decode(s):
    """
    Returns the ASCII decoded version of the given HTML string. This does
    NOT remove normal HTML tags like <p>.
    """
    htmlCodes = (
            ("'", '&#39;'),
            ('%', '%25'),
            ('"', '&quot;'),
            ('>', '&gt;'),
            ('<', '&lt;'),
            ('&', '&amp;')
        )
    for code in htmlCodes:
        s = s.replace(code[1], code[0])
    return s



def web_page():
    if rele.value() == 0:
        gpio_state = "ON"
    else:
        gpio_state = "OFF"

    html = """<html><head> <title>ESP Web Server</title> <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="data:,"> <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
    h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}.button{display: inline-block; background-color: #e7bd3b; border: none; 
    border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
    .button2{background-color: #4286f4;}.button3{background-color: #94C880;}</style></head><body><h1>ESP Web Server</h1> """
    try:
        f = open('wifi.txt')
        html = html + """
    <p>GPIO state: <strong>""" + gpio_state + """</strong></p><p><a href="/?led=on"><button class="button">ON</button></a></p>
    <p><a href="/?led=off"><button class="button button2">OFF</button></a></p>"""
    except:
        html = html + """
        <form action="/" method="GET">
        SSID: <input type="text" name="SSID" value=""><br>
        PASSWORD: <input type="text" name="password" value=""><br>
        <button class="button button2" type="submit" value="Salva"> Salva </button>
        </form>
        """
    html = html + """</body></html>"""
    return html


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    #print('Content trunked = %s' % request[:100])
    #print(request[:100].find('SSID='))
    if (request[:150].find('SSID=') > -1) and (request[:150].find('password=') > -1):
        match = ure.search("SSID=([^&]*)&password=(.*)", request[:100])
        SSID=html_decode(match.group(1))
        password=html_decode(str(match.group(2)).split()[0])
        print('crendetials to be written = {}|{}'.format(SSID,password))
        f = open('wifi.txt', 'w')
        f.write(SSID + "|" + password)
        f.close()
        d = open('wifi.txt', 'r')
        print('value = ' + d.read())
        d.close()
        response = web_page()
        conn.send(response)
        conn.close()
        machine.reset()
        #
    else:
        led_on = request.find('/?led=on')
        led_off = request.find('/?led=off')
        if led_on == 6:
            print('LED ON')
            rele.value(0)
        if led_off == 6:
            print('LED OFF')
            rele.value(1)
        print('invio pagina')
        response = web_page()
        conn.send(response)
        conn.close()